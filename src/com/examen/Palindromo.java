package com.examen;

public class Palindromo {
	
	public boolean identificarPalindromo(String cadena){
		boolean valor=true;
		int i,ind;
		String cadena2="";
		
		//quitamos los espacios
		
		for (int p=0; p < cadena.length(); p++) {
		if (cadena.charAt(p) != ' ')
		cadena2 += cadena.charAt(p);
		}
		
		//volvemos a asignar variables
		
		cadena=cadena2;
		ind=cadena.length();
		
		//comparamos cadenas
		for (i= 0 ;i < (cadena.length()); i++){
		if (cadena.substring(i, i+1).equals(cadena.substring(ind - 1, ind)) == false ) {
			
			
		//si una sola letra no corresponde no es un palindromo por tanto
		//sale del ciclo con valor false
			
		valor=false;
		break;
		}
		ind--;
		}
		return valor;
		}

}
